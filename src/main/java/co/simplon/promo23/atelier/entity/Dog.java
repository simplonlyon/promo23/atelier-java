package co.simplon.promo23.atelier.entity;

import java.util.Date;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;


public class Dog {
    
    private Integer id;
    @NotBlank
    private String name;
    @NotBlank
    private String breed;
    @PastOrPresent
    @NotNull
    private Date birthdate;

    public Dog(String name, String breed, Date birthdate) {
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public Dog(Integer id, String name, String breed, Date birthdate) {
        this.id = id;
        this.name = name;
        this.breed = breed;
        this.birthdate = birthdate;
    }
    public Dog() {
    }
    
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getBreed() {
        return breed;
    }
    public void setBreed(String breed) {
        this.breed = breed;
    }
    public Date getBirthdate() {
        return birthdate;
    }
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }
}
