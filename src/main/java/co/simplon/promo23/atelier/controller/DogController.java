package co.simplon.promo23.atelier.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo23.atelier.entity.Dog;
import co.simplon.promo23.atelier.repository.DogRepository;
import jakarta.validation.Valid;

@RestController
@RequestMapping("/api/dog")
public class DogController {
    @Autowired
    private DogRepository repo;


    @GetMapping("/example")
    public Dog first() {
        char lettre ='c';
        String phrase = "trucs";
        return new Dog(1, "Test", "Toust", new Date());
    }

    @GetMapping
    public List<Dog> all() {
        return repo.findAll();
    }

    @PostMapping
    public Dog add(@RequestBody @Valid Dog dog) {
        repo.persist(dog);
        return dog;

    }



    
}
