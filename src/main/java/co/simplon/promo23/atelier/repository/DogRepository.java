package co.simplon.promo23.atelier.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo23.atelier.entity.Dog;

@Repository
public class DogRepository {
    @Autowired
    private DataSource dataSource;


    public List<Dog> findAll() {
        List<Dog> list = new ArrayList<>();

        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");

            ResultSet results = stmt.executeQuery();
            while(results.next()) {
                Dog dog = new Dog(
                    results.getInt("id"),
                    results.getString("name"),
                    results.getString("breed"),
                    results.getDate("birthdate")
                );
                list.add(dog);
            }

        } catch (SQLException e) {
            System.err.println(e);
            e.printStackTrace();
        }
        

        return list;
    }

    public void persist(Dog dog) {
         try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog (name,breed,birthdate) VALUES (?,?,?)",  Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            stmt.setDate(3, new java.sql.Date(dog.getBirthdate().getTime()));

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()) {

                dog.setId(rs.getInt(1));
            }
           
        } catch (SQLException e) {
            System.err.println(e);
            e.printStackTrace();
        }
    }
}
